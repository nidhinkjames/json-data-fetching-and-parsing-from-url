package com.example.qube.webservices1;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class FetchData extends AsyncTask <Void, Void, Void> {
    String data = "";
    String singleParsed ="";
    String dataParsed="";
    @Override
    protected Void doInBackground(Void... voids) {
        try {
            URL url= new  URL("https://api.myjson.com/bins/o6u6q");
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            InputStream inputStream = httpURLConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
            String line = "";
            while (line != null) {
                line = bufferedReader.readLine();
                data = data + line;
            }

                JSONArray JA = new JSONArray(data);
                for(int i=0 ;i < JA.length(); i++){
                    JSONObject jo = (JSONObject) JA.get(i);
                    singleParsed = "Name :" +jo.get( "name" )+"\n"
                                     +"Age : "+jo.get("age")+"\n"
                                     +"Country : "+jo.get("Country")+"\n";
                    dataParsed = dataParsed + singleParsed + "\n";
                }


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }


    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        MainActivity.data.setText(this.dataParsed);

    }
}
